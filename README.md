# mzdbook

เป็นคู่มือการใช้งานและแก้ปัญหาเขียนจากประสบการณ์ผู้ใช้ มีความตั้งใจให้ความรู้คงอยู่เมื่อในอนาคตได้เลิกใช้รถไปแล้วหรือเลิกเห่อรถ ตัวอย่างเช่นแก้ปัญหาหน้าจอค้าง เป็นต้น

โค๊ดในนี้เป็นต้นทางของไฟล์ mzdbook.pdf เขียนด้วยภาษา LaTeX ไฟล์สำคัญคือ mzdbook.tex และ mzdbook.bib ที่เหลือเป็นไฟล์ที่สร้างเมื่อตอน compile 

เนื้อหาภายในหนังสือเกี่ยวข้องกับ การแนะนำวิธีใช้งานปลั๊กอิน และ วิธีแก้ไขเมื่อพบปัญหา


ติดตั้งโปรแกรม [TeXLive](https://www.tug.org/texlive) ใช้เป็นคอมไพล์เลอร์ TeX ภาษาไทยใช้ XeLaTeX 
ใช้โปรแกรม [Texstudio](http://www.texstudio.org/) เปิดและแก้ไฟล์ 

ปล. ผู้เขียนใช้ texpad บน Mac

# วิธีติดตั้ง fonts

* fonts Angsima ดาวน์โหลดที่ https://www.thaitux.info/node/275
```bash
wget https://www.thaitux.info/files/fonts/thaifont-abc.tar.gz
tar xf thaifont-abc.tar.gz 
mkdir ~/.fonts
cp thaifont-abc/*.ttf ~/.fonts
```
